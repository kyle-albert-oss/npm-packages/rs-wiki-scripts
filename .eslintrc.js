module.exports = {
	plugins: ["@typescript-eslint"],
	parser: "@typescript-eslint/parser",
	parserOptions: {
		project: "./tsconfig.eslint.json",
	},
	extends: [
		"eslint:recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:@typescript-eslint/recommended-requiring-type-checking",
		"prettier",
		"prettier/@typescript-eslint",
	],
	rules: {
		"prefer-const": [
			"warn",
			{
				destructuring: "all",
			},
		],
		"require-await": "off",
		"@typescript-eslint/ban-ts-comment": "warn",
		"@typescript-eslint/explicit-function-return-type": "off",
		"@typescript-eslint/no-inferrable-types": "off",
		"@typescript-eslint/no-unsafe-assignment": "warn",
		"@typescript-eslint/no-unsafe-call": "warn",
		"@typescript-eslint/no-unsafe-member-access": "warn",
		"@typescript-eslint/require-await": "warn",
	},
};
