import nodeResolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import typescript from "@rollup/plugin-typescript";
import { uglify } from "rollup-plugin-uglify";

import pkg from "./package.json";
import replace from "@rollup/plugin-replace";

const extensions = [".js", ".ts", ".tsx", ".json"];
const entryPoint = "src/index.ts";
const sourcemap = "inline";

const nodeResolveOpts = {
	builtins: false,
	extensions,
};

// order matters
const sharedPlugins = [
	replace({
		"Symbol.for": `Symbol["for"]`,
	}),
	nodeResolve(nodeResolveOpts),
	typescript({
		tsconfig: "./tsconfig.dist.json",
	}),
	commonjs({ extensions }),
    // uglify(),
].filter((p) => p != null);

const bundles = [
	{
		input: entryPoint,
		output: {
			name: "index",
			file: pkg.browser,
			format: "umd",
		},
		plugins: [...sharedPlugins],
	}
];

export default bundles;
