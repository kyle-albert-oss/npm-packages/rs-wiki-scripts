/* eslint-env commonjs */
/**
 * https://jestjs.io/docs/en/configuration
 */
module.exports = {
	globals: {
		"ts-jest": {
			tsConfig: "tsconfig.test.json",
		},
	},
	preset: "ts-jest",
	testEnvironment: "jsdom",
	moduleFileExtensions: ["js", "jsx", "ts", "tsx"],
	modulePathIgnorePatterns: ["<rootDir>/coverage/", "<rootDir>/dist/", "<rootDir>/out/"],
	//	setupFilesAfterEnv: ["./tests/setup-tests.ts"],
	transform: {
		"\\.(tsx?)$": "ts-jest",
	},
};
