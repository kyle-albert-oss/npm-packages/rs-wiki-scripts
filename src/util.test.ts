import { lastIsoWeekday, ISO_WEEKDAY_WEDNESDAY } from "./util";
import moment from "moment";

describe("lastIsoWeekday", () => {
	it("returns Wednesday of last week if passed in date is before this week's Wednesday.", () => {
		const time = moment.utc("2020-05-26 01:23");
		const result = lastIsoWeekday(time, ISO_WEEKDAY_WEDNESDAY);

		expect(result.year()).toEqual(2020);
		expect(result.month() + 1).toEqual(5);
		expect(result.date()).toEqual(20);
		expect(result.hour()).toEqual(0);
		expect(result.minute()).toEqual(0);
	});

	it("returns Wednesday of this week if passed in date is this week's Wednesday.", () => {
		const time = moment.utc("2020-05-27");
		const result = lastIsoWeekday(time, ISO_WEEKDAY_WEDNESDAY);

		expect(result.year()).toEqual(2020);
		expect(result.month() + 1).toEqual(5);
		expect(result.date()).toEqual(27);
		expect(result.hour()).toEqual(0);
		expect(result.minute()).toEqual(0);
	});

	it("returns Wednesday of this week if passed in date after this week's Wednesday.", () => {
		const time = moment.utc("2020-05-31 01:23");
		const result = lastIsoWeekday(time, ISO_WEEKDAY_WEDNESDAY);

		expect(result.year()).toEqual(2020);
		expect(result.month() + 1).toEqual(5);
		expect(result.date()).toEqual(27);
		expect(result.hour()).toEqual(0);
		expect(result.minute()).toEqual(0);
	});
});
