import { log, getMeta } from "./util";
import { processChecklists } from "./checklists";

log("Initializing...");
setTimeout(() => {
	const pageName = getMeta("og:title");
	log("Page name:", pageName);
	if (!pageName) {
		log("No page name detected, aborting script.");
		return;
	}
	if (pageName.indexOf("Storm/Game/Repeatable Tasks") !== -1) {
		log("Processing checklists...");
		processChecklists();
	}
	log("DONE.");
}, 1000);

