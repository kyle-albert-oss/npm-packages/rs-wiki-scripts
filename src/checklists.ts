import moment from "moment";
import { clickResetIfExpired, lastIsoWeekday, ISO_WEEKDAY_WEDNESDAY } from "./util";

export const processChecklists = () => {
	const LS_DAILY_CLEARED_KEY = "ka-dailyCleared";
	const LS_WEEKLY_CLEARED_KEY = "ka-weeklyCleared";
	const LS_MONTHLY_CLEARED_KEY = "ka-monthlyCleared";
	const ID_DAILIES_TABLE = "storm-dailies";
	const ID_WEEKLIES_TABLE = "storm-weeklies";
	const ID_MONTHLY_TABLE = "storm-monthlies";

	const now = moment.utc();

	// check if we should reset dailies
	const dailyClearTime = moment.utc().startOf("day");
	const lsDailyCleared = localStorage.getItem(LS_DAILY_CLEARED_KEY);
	if (clickResetIfExpired(lsDailyCleared, dailyClearTime, ID_DAILIES_TABLE)) {
		localStorage.setItem(LS_DAILY_CLEARED_KEY, now.format());
	}

	// check weeklies
	const weeklyClearTime = lastIsoWeekday(now, ISO_WEEKDAY_WEDNESDAY);
	const lsWeeklyCleared = localStorage.getItem(LS_WEEKLY_CLEARED_KEY);
	if (clickResetIfExpired(lsWeeklyCleared, weeklyClearTime, ID_WEEKLIES_TABLE)) {
		localStorage.setItem(LS_WEEKLY_CLEARED_KEY, now.format());
	}

	// check monthlies
	const monthlyClearTime = moment.utc().startOf("month");
	const lsMonthlyCleared = localStorage.getItem(LS_MONTHLY_CLEARED_KEY);
	if (clickResetIfExpired(lsMonthlyCleared, monthlyClearTime, ID_MONTHLY_TABLE)) {
		localStorage.setItem(LS_MONTHLY_CLEARED_KEY, now.format());
	}
};
