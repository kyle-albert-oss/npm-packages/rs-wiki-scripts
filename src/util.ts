import moment, { Moment } from "moment";

export const log = (...items: any[]) => {
	console.log("[User:Storm/common.js]", ...items);
};
export const error = (...items: any[]) => {
	console.error("[User:Storm/commonjs]", ...items);
};

export const getMeta = (name: string) => {
	const qr = document.querySelector<HTMLMetaElement>(`meta[property="${name}"]`);
	return qr != null ? qr.content : undefined;
};

export const ISO_WEEKDAY_WEDNESDAY = 3;
export const lastIsoWeekday = (time: Moment, isoWeekday: number): Moment => {
	let result: Moment = moment(time);
	if (time.isoWeekday() < isoWeekday) {
		result = result.subtract(1, "week");
	}
	return result.startOf("isoWeek").weekday(isoWeekday);
};

export const clickReset = (tableId: string) => {
	const resetBtn = document.querySelector<HTMLAnchorElement>(`#${tableId} a[title="Removes all highlights from the table"]`);
	if (resetBtn) {
		resetBtn.click();
		log(`${tableId} table reset clicked.`);
	} else {
		error(`${tableId} table could not be reset.`);
	}
};

export const clickResetIfExpired = (lastCleared: string | null, resetTime: Moment, tableId: string): boolean => {
	if (!lastCleared) {
		return true;
	} else {
		const lastClearedMmt = moment(lastCleared);
		if (lastClearedMmt.isValid()) {
			if (lastClearedMmt.isBefore(resetTime)) {
				clickReset(tableId);
				return true;
			}
		} else {
			return true;
		}
	}
	return false;
};
